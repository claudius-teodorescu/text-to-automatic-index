executable_name=$(cargo metadata --no-deps --format-version 1 | jq -r '.packages[0] | .name')
target="x86_64-unknown-linux-musl"

version_number=$(cargo pkgid | cut -d "#" -f2)

rustup target add x86_64-unknown-linux-musl
cargo build --target-dir ./target --release --target $target

mkdir public

cp ./target/$target/release/$executable_name ./public/

curl --location --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
           --upload-file ./target/$target/release/${executable_name} \
           "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${executable_name}/$version_number/${executable_name}"
