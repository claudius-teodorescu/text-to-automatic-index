use clap::{Arg, Command};
use glob::Pattern;
use rio_api::formatter::TriplesFormatter;
use rio_api::model::{Literal, NamedNode, Triple};
use rio_turtle::TurtleFormatter;
use std::collections::{HashMap, HashSet};
use std::path::{Path, PathBuf};
use std::{env, fs};
use diacritics;

use crate::tokenizer::{Token, Tokenizer, WhitespaceTokenizer};

mod tokenizer;

fn main() {
    // get the CLI args
    let cli_matches = Command::new(env!("CARGO_PKG_NAME"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .version(env!("CARGO_PKG_VERSION"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .arg(
            Arg::new("text_files_base_path")
                .value_parser(clap::value_parser!(PathBuf))
                .index(1)
                .required(true),
        )
        .arg(Arg::new("text_files_glob").index(2).required(true))
        .arg(
            Arg::new("indexes_dir")
                .value_parser(clap::value_parser!(PathBuf))
                .index(3)
                .required(true),
        )
        .arg(
            Arg::new("texts_base_iri")
                .value_parser(clap::value_parser!(String))
                .index(4)
                .required(true),
        )
        .arg(
            Arg::new("terms_base_iri")
                .value_parser(clap::value_parser!(String))
                .index(5)
                .required(true),
        )
        .arg(
            Arg::new("normalize_text")
                .value_parser(clap::value_parser!(bool))
                .index(6)
                .required(true),
        )
        .get_matches();

    let text_files_base_path: &PathBuf = cli_matches
        .get_one("text_files_base_path")
        .expect("`text_files_base_path` is required");
    let text_files_glob_arg: &String = cli_matches
        .get_one("text_files_glob")
        .expect("`text_files_glob` is required");
    let text_files_glob: Pattern =
        Pattern::new(text_files_glob_arg.as_str()).expect("invalid `text_files_glob`");
    let indexes_dir: &PathBuf = cli_matches
        .get_one("indexes_dir")
        .expect("`indexes_dir` is required");
    let texts_base_iri: &String = cli_matches
        .get_one("texts_base_iri")
        .expect("`texts_base_iri` is required");
    let terms_base_iri: &String = cli_matches
        .get_one("terms_base_iri")
        .expect("`terms_base_iri` is required");
    let normalize_text: &bool = cli_matches
        .get_one("normalize_text")
        .expect("`normalize_text` is required");

    let text_files_path = text_files_base_path.join(text_files_glob.as_str());

    let mut global_terms: HashMap<String, String> = HashMap::new();
    let mut text_relative_iri_formatter = TurtleFormatter::new(Vec::default());
    let mut terms_formatter = TurtleFormatter::new(Vec::default());
    let mut terms_formatter_2 = TurtleFormatter::new(Vec::default());

    for (file_ordinal_number, text_file_path) in glob::glob(text_files_path.to_str().unwrap())
        .expect("Failed to read glob pattern")
        .filter_map(Result::ok)
        .enumerate()
    {
        // initialize the structure for terms with aggregated positions
        let mut aggregated_file_terms: HashMap<String, String> = HashMap::new();

        // generate the relative file path and add it to the global struct
        let text_file_relative_path = get_relative_path(text_files_base_path, &text_file_path);
        text_relative_iri_formatter
            .format(&Triple {
                subject: NamedNode {
                    iri: text_file_relative_path.as_str(),
                }
                .into(),
                predicate: NamedNode {
                    iri: "dc:identifier",
                }
                .into(),
                object: Literal::Simple {
                    value: file_ordinal_number.to_string().as_str(),
                }
                .into(),
            })
            .expect("error formatting text relative IRIs triple");

        // get the file contents
        let mut file_contents: String = fs::read_to_string(&text_file_path)
            .expect("cannot read file")
            .parse()
            .expect("cannot parse file");

        // normalize the contents is asked for
        if *normalize_text {
            file_contents = diacritics::remove_diacritics(file_contents.as_str());
        }
        // get the contents' tokens
        let file_tokens = WhitespaceTokenizer
            .tokenize(&file_contents)
            .collect::<Vec<Token>>();

        // aggregate the tokens
        let mut unique_file_tokens: HashSet<String> = HashSet::new();
        file_tokens.iter().for_each(|file_token| {
            let term = file_token.term();
            let position = file_token.position();
            aggregated_file_terms
                .entry(term.to_owned())
                .and_modify(|entry| {
                    *entry = format!("{},{}", *entry, position);
                })
                .or_insert(format!("\"{}\":[{}", file_ordinal_number, position));

            unique_file_tokens.insert(term.to_string());
        });

        // add the file terms to global terms' struct
        aggregated_file_terms.iter().for_each(|file_term| {
            let term = file_term.0;
            let mut positions = file_term.1.to_string();

            if !positions.ends_with("]") {
                positions = format!("{}]", positions);
            }

            global_terms
                .entry(term.to_owned())
                .and_modify(|entry| {
                    *entry = format!("{},{}", *entry, positions);
                })
                .or_insert(format!("{}", positions));
        });

        for unique_file_token in unique_file_tokens.iter() {
            terms_formatter_2
                .format(&Triple {
                    subject: NamedNode {
                        iri: text_file_relative_path.as_str(),
                    }
                    .into(),
                    predicate: NamedNode { iri: "i:word" }.into(),
                    object: Literal::Simple {
                        value: unique_file_token,
                    }
                    .into(),
                })
                .expect("error formatting text relative IRIs triple");
        }
    }

    // generate the triples file for text relative IRIs
    let mut text_relative_iri_prolog = format!(
        "@base <{}> .\n{} .\n",
        texts_base_iri, "@prefix dc: <http://purl.org/dc/terms/>"
    )
    .as_bytes()
    .to_vec();
    let mut text_relative_iri_document = text_relative_iri_formatter
        .finish()
        .expect("cannot finish text relative IRIs generation");
    text_relative_iri_prolog.append(&mut text_relative_iri_document);
    let text_relative_iris_file_dir_path = indexes_dir.join("text-relative-iris");
    fs::create_dir_all(&text_relative_iris_file_dir_path).unwrap();
    fs::write(
        text_relative_iris_file_dir_path.join("text-relative-iris.ttl"),
        &text_relative_iri_prolog,
    )
    .expect("Write file.");

    // generate the triples file for terms
    let mut terms_prolog = format!(
        "@base <{}> .\n{} .\n",
        terms_base_iri, "@prefix bibo: <http://purl.org/ontology/bibo/>"
    )
    .as_bytes()
    .to_vec();
    global_terms.iter().for_each(|global_term| {
        terms_formatter
            .format(&Triple {
                subject: NamedNode {
                    iri: global_term.0.as_str(),
                }
                .into(),
                predicate: NamedNode {
                    iri: "bibo:locator",
                }
                .into(),
                object: Literal::Simple {
                    value: format!("{{{}}}", global_term.1).as_str(),
                }
                .into(),
            })
            .expect("error formatting text relative IRIs triple");
    });
    let mut terms_document = terms_formatter
        .finish()
        .expect("cannot finish terms generation");
    terms_prolog.append(&mut terms_document);
    let terms_file_dir_path = indexes_dir.join("terms");
    fs::create_dir_all(&terms_file_dir_path).unwrap();
    fs::write(terms_file_dir_path.join("terms.ttl"), &terms_prolog).expect("Write file.");

    let mut terms_prolog_2 = format!(
        "@base <{}> .\n{}\n",
        terms_base_iri, "@prefix i: <https://kuberam.ro/ontologies/text-index#> ."
    )
    .as_bytes()
    .to_vec();
    let mut terms_document_2 = terms_formatter_2
        .finish()
        .expect("cannot finish terms generation");
    terms_prolog_2.append(&mut terms_document_2);
    let terms_file_dir_path = indexes_dir.join("terms");
    fs::create_dir_all(&terms_file_dir_path).unwrap();
    fs::write(terms_file_dir_path.join("terms_2.ttl"), &terms_prolog_2).expect("Write file.");
}

fn get_relative_path(text_files_base_path: &Path, text_file_path: &PathBuf) -> String {
    let mut text_file_relative_path_buffer = PathBuf::new();
    text_file_path
        .components()
        .skip(text_files_base_path.components().collect::<Vec<_>>().len())
        .for_each(|component| text_file_relative_path_buffer.push(component));
    let text_file_relative_path = text_file_relative_path_buffer.to_str().unwrap();

    text_file_relative_path.to_owned()
}

#[test]
fn normalization() {
    let normalized_string = diacritics::remove_diacritics("TÅRÖÄÆØ");

    println!("{}", normalized_string);
}

// time ./text-data-to-triples "/home/claudius/workspace/repositories/git/github.com/wujastyk/INDOLOGY-forum-data/data/" "**/*.txt" "/home/claudius/workspace/repositories/git/github.com/wujastyk/INDOLOGY-forum-data/public/datasets/" "https://wujastyk.github.io/INDOLOGY-forum-data/indexes/words-fulltext/" "https://wujastyk.github.io/INDOLOGY-forum-data/data/"

// RESOURCES
// detect encoding of file, https://stackoverflow.com/questions/805418/how-can-i-find-encoding-of-a-file-via-a-script-on-linux
// get content of non-UTF8 file in Rust, https://stackoverflow.com/questions/61221763/how-can-i-get-the-content-of-a-file-if-it-isnt-contain-a-valid-utf-8
